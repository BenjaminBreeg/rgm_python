__author__ = 'whisper'
from numpy import absolute, array
from scipy.signal import firwin
from sympy import cos
from math import fabs, pi, sqrt
from scipy.signal import butter, filtfilt
import math
from support import integrate
from filters import filter_90hz, filter_150hz


def  absol(sample):
    if sample < 0:
        sample = 0
    return sample


def am_demod(sample, time, fc):
    lol = 0
    size = int(sample.size)
    fs = 256e4
    # 2 560 000
    time /= fs
    while lol < size:
        sample[lol] = absol(sqrt(sample[lol]*sample[lol].conj())*cos(fc*(time*lol)))
        # sample[lol] = absol(sqrt((sample[lol]*sample[lol].conj())))
        # print(sample[lol])
        lol += 1
    # b, a = butter(N=5, Wn=0.00078125, btype='high') #todo do a lowpass filter
    # sample = filtfilt(b, a, sample)
    return sample.real


def find_average(sample):
    average = integrate(sample)/int(sample.size)
    return average


def calculate_deep_90hz(signal, time, fc, deep):
    lol = 0
    time /= 256e4
    signal = filter_90hz(signal)
    while lol < int(signal.size):
        signal[lol] = (signal[lol]*signal[lol].conj()*cos(3*fc*(time*lol)))
        lol += 1
    mod = 2*pi*integrate(signal)/deep
    return mod


def calculate_deep_150hz(signal, time, fc, deep):
    lol = 0
    time /= 256e4
    signal = filter_150hz(signal)
    while lol < int(signal.size):
        signal[lol] = (signal[lol]*signal[lol].conj()*cos(5*fc*(time*lol)))
        lol += 1
    mod = 2*pi*integrate(signal)/deep
    return mod


def calculate(signal, time, fc):
    deep = integrate(signal)/int(signal.size)
    m90hz = calculate_deep_90hz(signal, time, fc, deep)
    m150hz = calculate_deep_150hz(signal, time, fc, deep)
    print("LoL")
    print(deep)
    print(m90hz)
    print(m150hz)
    rgm = m90hz - m150hz
    sgm = m90hz + m150hz
    result = {}
    result.update(
        rgm=rgm,
        sgm=sgm,
    )
    return result