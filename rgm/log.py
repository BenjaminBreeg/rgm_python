__author__ = 'whisper'
import datetime
from geoposition import get_gps, get_height


#todo deprecated
def write_to_file_d(sample):
    log_file = open("log.txt", mode='a')
    time = datetime.datetime.now().strftime("%d.%m.%Y %I:%M %p")
    # gps = get_gps('/dev/ttyUSB0')
    height = get_height()
    log_file.write(" time: " + time + str(height))
    log_file.write(sample)


def write_to_file(sample, gain, id):
    log_file_name = "log_" + str(id)
    log_file = open(log_file_name, mode='a')
    gps = get_gps('/dev/ttyUSB0')
    time = datetime.datetime.now().strftime("%d.%m.%Y %I:%M %p")
    log_file.write("time: " + time + " gain: " + gain
                   + " latitude: " + gps.__getitem__('latitude')
                   + " longitude: " + gps.__getitem__('longitude')
                   + " altitude: " + gps.__getitem__('altitude'))
    log_file.write(sample)
    log_file.write("end_of_sample")


def write_to_file_z(calculations, gain, id):
    log_file_name = "log.txt"
    log_file = open(log_file_name, mode='a')
    gps = get_gps('/dev/ttyUSB0')
    time = datetime.datetime.now().strftime("%I %M %p")
    log_file.write(str(gps.__getitem__('latitude')))
    log_file.write(str(gps.__getitem__('longitude')))
    log_file.write(str(gps.__getitem__('altitude')))
    log_file.write(time)
    log_file.write(str(calculations.__getitem__('rgm')))
    log_file.write(str(calculations.__getitem__('sgm')))
    log_file.write(str(gain))
    log_file.write("\r\n")


def write_to_file_h(sample):
    sample_size = sample.size
    log_file = open("log_b.txt", mode='a')
    lol = 0
    gps = get_gps('/dev/ttyUSB0')
    line = "g "
    line += str(gps.__getitem__('latitude'))
    line += " "
    line += str(gps.__getitem__('longitude'))
    line += " "
    line += str(gps.__getitem__('altitude'))
    # line += " "
    # line += datetime.datetime.now().strftime("%I %M")
    line += ";\r\n"
    log_file.writelines(line)
    while lol < sample_size:
        line = "s "
        line += (str(sample[lol].real))
        line += " "
        line += str(sample[lol].imag)
        line += ";\r\n"
        log_file.writelines(line)
        print line.__sizeof__()
        lol += 1
    log_file.close()


def write_to_file_hd(sample):
    sample_size = sample.size
    log_file_name = "log_"
    log_file_name += datetime.datetime.now().strftime("%H_%M")
    log_file_name += ".txt"
    log_file = open(log_file_name, mode='a')
    lol = 0
    gps = get_gps('/dev/ttyUSB0')
    line = "g "
    line += str(gps.__getitem__('latitude'))
    line += " "
    line += str(gps.__getitem__('longitude'))
    line += " "
    line += str(gps.__getitem__('altitude'))
    line += ";\r\n"
    log_file.writelines(line)
    while lol < sample_size:
        line = "s "
        line += (str(sample[lol].real))
        line += " "
        line += str(sample[lol].imag)
        line += ";\r\n"
        log_file.writelines(line)
        lol += 1
    log_file.close()