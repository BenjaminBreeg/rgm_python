__author__ = 'whisper'
from numpy import ndarray
from scipy.signal import decimate


#todo exclude from program: eat a lot of memory
def decimate_by(sample, decimator):
    # dec_sample = ndarray(shape=sample.shape, dtype=sample.dtype)
    # lol = 0
    # ololo = 0
    # while ololo < sample.size:
    #     dec_sample[lol] = sample[ololo]
    #     ololo += decimator
    #     lol += 1
    return decimate(x=sample, q=decimator)


#todo make it work
def auto_gain_reg(gain, average, is_grm):
    step = 1
    if is_grm:
        if average < 0.5:
            gain += step
        elif average == 0.8:
            return gain
        else:
            gain -= step
    else:
        if average < 0.4:
            gain += step
        elif average == 0.4:
            return gain
        else:
            gain -= step
    print(gain)
    return gain


def integrate(sample):
    size = int(sample.size)
    lol = 0
    lor = size - 2
    sl = 0
    sr = 0
    while lol < size/2:
        sl += sample[lol]
        sr += sample[lor]
        lol += 1
        lor -= 1
    return sl+sr


def integrate_d(sample):
    size = int(sample.size)
    lol = 0
    s = 0
    while lol < size:
        s += sample[lol].real
        lol += 1
        print s
    return s