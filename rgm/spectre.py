from scipy import signal
from scipy.fftpack import fft


def recalcAreas(low, high)
	context()
	context.update(
		low = int(2*Pi*low),
		high = int(2*Pi*high)
	)
	return context

	
def rgmCalc(signal, time)
	grm = 330e6
	krm = 144e6
	lowfreq = 90
	highfreq = 150
	back = fft(signal)
	back_len = back.__lenght
	lfperiod = recalcAreas(lowfreq - 10,lowfreq + 10)
	rfperiod = recalcAreas(highfreq - 10,highfreq + 10)
	iter = 0
	low_buf = 0
	low_count = 0
	high_buf = 0
	high_count = 0
	context()
	while(iter < back_len) #todo freq areas autocomplite
		iter += 1
		if iter > lfperiod.get('low') and iter < lfperiod.get('high')
			low_buf += back[iiter]
			low_count += 1
		if iter > rfperiod.get('low') and iter < rfperiod.get('high')
			high_buf += back[iter]
			high_count += 1
		if iter > lfperiod.get('high') and iter > rfperiod.get('high')
			break
	M_common = (low_buf + high_buf)/(low_count + high_count)
	M_low = low_buf / low_count
	M_high = high_buf / high_count
	mlow = M_low / M_common
	mhigh = M_high / M_common
	context.update(
		rgm = mhigh - mlow,
		sgm = mhigh + mlow,
		mlow = mlow,
		mhigh = mhigh,
		m_common = M_common,
	)
	return context