__author__ = 'whisper'
import serial

#solved
#todo test bytes coords
def get_gps(port):
    gps = {}
    try:
        uart = serial.Serial(port=port, baudrate=4800, timeout=1)
        u_line = uart.readline()
        latitude = u_line[18:26]
        longitude = u_line[31:39]
        altitude = u_line[34:42]
        gps.update(
            latitude=latitude,
            longitude=longitude,
            altitude=altitude,
        )
    except:
        gps.update(
            latitude=10,
            longitude=10,
            altitude=10,
        )
    return gps


#todo deprecated
def get_height():
    height = 10
    return height
