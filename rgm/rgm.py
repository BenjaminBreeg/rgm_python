__author__ = 'whisper'
from rtlsdr import *
from log import write_to_file_hd
import time
from scipy.signal import decimate
import RPIO
# import RPi.GPIO as gpio


def main_logger():
    NRF_CE = 4
    # RPIO.setup(NRF_CE, RPIO.OUT, initial=RPIO.LOW)
    RPIO.cleanup()
    # RPIO.setmode(RPIO.BOARD)
    RPIO.setup(NRF_CE, RPIO.OUT)
    RPIO.output(NRF_CE, 0)
    while True:
        print("start log writing")
        RPIO.output(NRF_CE, 0)
        try:
            sdr = RtlSdr()
        except IOError, e:
            RPIO.output(NRF_CE, 0)
        if sdr is None:
            print("Oh my god, we still find dongle")
            time.sleep(1)
        else:
            lol = 1
            # time.sleep(30)
            while True:
                try:
                    RPIO.output(NRF_CE, 1)
                    sdr.sample_rate = 256e4
                    sdr.center_freq = 111e6
                    sdr.set_manual_gain_enabled(True)
                    sample = sdr.read_samples(1024*100)
                    # gain = sdr.get_gain()
                    sample = decimate(sample, 8)
                    write_to_file_hd(sample)
                    lol += 1
                    print(lol)
                    RPIO.output(NRF_CE, 0)
                except IOError:
                    RPIO.output(NRF_CE, 0)
                    exit()
                except UnboundLocalError:
                    RPIO.output(NRF_CE, 0)
                    exit()
                except RuntimeError, e:
                    RPIO.output(NRF_CE, 0)
                    exit()
                except KeyboardInterrupt, e:
                    RPIO.output(NRF_CE, 0)
                    exit()
            RPIO.output(NRF_CE, 0)
        print("Ooops, Houston, we have a problem")
        print("waiting for device")
        time.sleep(2)


if __name__ == '__main__':
    while True:
        main_logger()
