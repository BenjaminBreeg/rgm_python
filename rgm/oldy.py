__author__ = 'whisper'
from rtlsdr import *
from log import write_to_file, write_to_file_d, write_to_file_z, write_to_file_h, write_to_file_hd
from demod import am_demod, find_average, calculate
import matplotlib.pyplot as plt
import time
from support import decimate_by, auto_gain_reg


def main_t():
    center_freq = 300e6
    sample_rate_freq = 256e4
    gain = 18
    sdr = RtlSdr()
    if sdr is None:
        raise ValueError("Fail")
    lol = 1
    while True:
        sdr.sample_rate = sample_rate_freq
        sdr.center_freq = center_freq
        sdr.set_direct_sampling(1)
        # sdr.freq_correction = 0.0004   # todo change if need
        sdr.gain = gain
        time_el = time.clock()
        sample = sdr.read_samples(2048)
        if lol > 5:
            time_el -= time.clock()
            # write_to_file_z(result, gain, lol)
            if DEBUG:
                plt.plot(sample)
                if lol > 15:
                    return 0
        lol += 1
    # if DEBUG:
        # plt.show()#todo deploy


def main_d():
    center_freq = 300e6
    sample_rate_freq = 256e4
    gain = 18
    sdr = RtlSdr()
    if sdr is None:
        raise ValueError("Fail")
    lol = 1
    while True:
        sdr.sample_rate = sample_rate_freq
        sdr.center_freq = center_freq
        # sdr.set_direct_sampling(1)
        # sdr.freq_correction = 0.0004   # todo change if need
        sdr.gain = gain
        time_el = time.clock()
        sample = sdr.read_samples(2048)
        if lol > 1:
            time_el -= time.clock()
            # sample = decimate_by(sample=sample, decimator=8)
            sample = am_demod(sample, time=time_el, fc=300e6)
            average = find_average(sample)
            gain = auto_gain_reg(gain, average, True)
            result = calculate(sample, time_el, center_freq)
            write_to_file_z(result, gain, lol)
            if DEBUG:
                if lol > 10:
                    return 0
                print(result.__getitem__('rgm'))
                # print("sgm=" + str(result.__getitem__('sgm')))
                # print(time_el)
                # print(sample)
                # plt.plot(sample) #todo deploy
        lol += 1
    # if DEBUG:
        # plt.show()#todo deploy


#todo old
def main():
    sdr = RtlSdr()
    if sdr is None:
        raise ValueError("Fail")
    lol = 1
    while lol < 2:
        sdr.sample_rate = 256e4  # todo rate in hz
        sdr.center_freq = 300e6     # todo freq in hz, ask!!!
        # sdr.freq_correction = 60   # todo i dont know what is it
        # sdr.gain = 0
        sdr.set_manual_gain_enabled(True) #autogain setup
        # sdr.set_direct_sampling(2)
        # time_el = time.clock()
        sample = sdr.read_samples(1024*70)
        # time_el -= time.clock()
        # sample = decimate_by(sample=sample, decimator=8)
        # sample = am_demod(sample, time=time_el, fc=300e6)
        # write_to_file_d(sample)
        # print(sample)
        write_to_file_h(sample)
        plt.plot(sample) #todo deploy
        lol += 1
    plt.show()#todo deploy
