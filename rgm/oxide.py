__author__ = 'whisper'
from rtlsdr import *
from log import write_to_file, write_to_file_d, write_to_file_z, write_to_file_h
from demod import am_demod, find_average, calculate
import matplotlib.pyplot as plt
import time
from support import decimate_by, auto_gain_reg


def main():
    center_freq = 300e6
    sample_rate_freq = 256e4
    gain = 0
    sdr = RtlSdr()
    if sdr is None:
        raise ValueError("Fail")
    lol = 1
    while lol < 10:
        sdr.sample_rate = sample_rate_freq
        sdr.center_freq = center_freq
        sdr.set_direct_sampling(1)
        # sdr.freq_correction = 0.0004   # todo change if need
        sdr.gain = gain
        time_el = time.clock()
        sample = sdr.read_samples(2048*20)
        print(time.clock()-time_el)
        plt.plot(sample)
        if lol > 4:
            write_to_file_h(sample)
        lol += 1
    plt.show()#todo deploy



if __name__ == '__main__':
    main()