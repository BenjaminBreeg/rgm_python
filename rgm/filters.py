__author__ = 'whisper'
from scipy.signal import firwin, remez, kaiser_atten, kaiser_beta, buttord, butter, filtfilt


#todo from 80hz to 100hz
def filter_90hz(signal):
    nyq = 128e4
    N, wn = buttord(wp=[80/nyq, 100/nyq], ws=[70/nyq, 110/nyq], gpass=3, gstop=40)
    b, a = butter(N, wn, btype='bandpass')
    return filtfilt(b, a, signal)


#todo from 140hz to 160hz
def filter_150hz(signal):
    nyq = 128e4
    N, wn = buttord(wp=[140/nyq, 160/nyq], ws=[130/nyq, 170/nyq], gpass=3, gstop=40)
    b, a = butter(N, wn, btype='bandpass')
    return filtfilt(b, a, signal)